package TestSuiteS4N;

import S4N.classes.Operaciones;
import S4N.classes.Oportunidad;
import S4N.classes.Pedido;
import org.junit.Test;

import java.util.*;
import java.util.function.IntPredicate;
import java.util.function.ToDoubleBiFunction;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

public class TestSuiteS4N {

    ArrayList<Oportunidad> misOportunidades;
    Oportunidad op1,op2,op3,op4,op5,op6;
    Operaciones op;
    Pedido ped1,ped2,ped3,ped4,ped5;

    public TestSuiteS4N(){

        ped1= new Pedido("PEDBOG-01-18",12000, "Alberto");
        ped2= new Pedido("PEDBAQ-02-18",45000, "Luis");
        ped3= new Pedido("PEDBUC-02-18",72000, "Carlos");
        ped4= new Pedido("PEDBUC-05-18",2705000, "Marcela");
        ped5= new Pedido("PEDBUC-15-18",134800, "Marcela");

        op1=new Oportunidad("BOG-01-18","Maria",35000,10,null);
        op2=new Oportunidad("BOG-02-18","Andrea",2300,25,null);
        op3=new Oportunidad("BOG-03-18","Carolina",135000,25,ped1);
        op4=new Oportunidad("BAQ-01-18","Jose",9000,50,ped2);
        op5=new Oportunidad("BUC-01-18","Fernando",109000,30,ped3);
        op6=new Oportunidad("BUC-04-18","Jose",25000,10,ped4);

        op=new Operaciones();
        misOportunidades=new ArrayList<>(Arrays.asList(op1,op2,op3,op4,op5,op6));
    }

    @Test
    public void numOportunidadesBogota(){
        long numOpptysBogota=misOportunidades.stream()
                .filter(op->op.getNumOportunidad().
                startsWith("BOG")).
                count();
        assertEquals(3,numOpptysBogota);
    }

    @Test
    public void numPedidosBq(){
        List<Oportunidad> pedidosBaq=misOportunidades.stream()
                .filter(op->op.getPedido().isPresent())
                .filter(op->op.getPedido().get()
                        .getNumPedido().contains("BAQ"))
                .collect(Collectors.toList());
        assertEquals(1,pedidosBaq.size());
    }

    @Test
    public void oportunidades2017(){
        boolean opty2017=misOportunidades.stream()
                .filter(op->op.getNumOportunidad().substring(7,8).equals("18"))
                .collect(Collectors.toList()).isEmpty();
        assertTrue(opty2017);
    }

    @Test
    public void obtenerPedidoNuloOportunidad(){
        Optional<Oportunidad> optionalOportunidad=Optional.ofNullable(op1);//pedido=nulo;
        Optional<Pedido> pedidoOptional=optionalOportunidad.map(Oportunidad::getPedido)
                .orElseThrow(NullPointerException::new);
        assertEquals("Optional.empty",pedidoOptional.toString());
    }

    @Test
    public void verificarPedidos12Caracteres(){
        int longitud=12;
        for(Oportunidad oportunidad:misOportunidades.stream()
                .filter(op->op.getPedido()
                        .isPresent())
                .collect(Collectors.toList())){
            Optional<Pedido> pedido=oportunidad.getPedido();
            int longitudPedido=pedido.get().getNumPedido().length();
            assertEquals(longitud,longitudPedido);
        }
    }

    @Test
    public void actualizarEmpleadoPedido(){
        String newName,oldName="";
        //for(Oportunidad oportunidad:misOportunidades){
            Optional<Oportunidad>optionalOportunidad=Optional.of(op3);
            oldName=optionalOportunidad.flatMap(Oportunidad::getPedido)
                    .flatMap(Pedido::getEmpleado)
                    .orElse("No encontrado");
            newName=optionalOportunidad.flatMap(Oportunidad::getPedido)
                    .flatMap(Pedido::getEmpleado)
                    .map(nombre->nombre.replace(nombre,"S4N Pruebas"))
                    .orElse("No encontrado");
        //System.out.println(oldName);
        //System.out.println(newName);
        assertThat(oldName,is(not(newName)));
    }


    @Test
    public void nombreVendedoresMayuscula(){
        Operaciones operaciones=new Operaciones();
        ArrayList<String> expectedArrayList=new ArrayList<>(Arrays.asList("MARIA","ANDREA","CAROLINA","JOSE","FERNANDO","JOSE"));
        ArrayList<String> ven2MayusArrayList=operaciones
                .empToMayus(misOportunidades,nombreVendedor->nombreVendedor.toUpperCase());
        assertEquals(expectedArrayList,ven2MayusArrayList);
    }

    @Test
    public void valorPedidoCuadrado(){
        ToDoubleBiFunction<Double,Integer> expresion=(valor,factor)->Math.pow(valor,factor);
        double valorPedido=ped1.getValorPedido();
        double cuadrado=expresion.applyAsDouble(valorPedido, 2);
        assertEquals(1.44E8,cuadrado);
        //System.out.println(cuadrado);
    }
   //--s---
    @Test
    public void listaPedidosBucaramanga(){

        List<String> pedidosBucExpected=Arrays.asList("PEDBUC-05-18","PEDBUC-02-18","PEDBUC-15-18");
        List<String> pedidosBuc=Arrays.asList(ped1,ped2,ped3,ped4,ped5).stream()
                              .filter(ped->ped.getNumPedido().contains("BUC"))
                              .map(Pedido::getNumPedido)
                              .collect(Collectors.toList());
        pedidosBuc.forEach(numPedido->{
           assertTrue(pedidosBucExpected.contains(numPedido));
        });
    }

    @Test
    public void sumaTotalPedidosDeOppty(){
        double sumaPedidosDeOptty=2834000;
        double sumaPedidos=misOportunidades.stream().filter(op->op.getPedido().isPresent())
                           .map(op->op.getPedido().get().getValorPedido())
                           .collect(Collectors.summingDouble(p->p));
        //System.out.println(Stream.of(1,2,3,4).collect(Collectors.summingInt(p->p)));
        assertEquals(sumaPedidosDeOptty,sumaPedidos);
    }

    @Test
    public void opptysPorExito(){
        Map<Integer,List<Oportunidad>> opptysPorExito=misOportunidades.stream()
                .collect(Collectors.groupingBy(Oportunidad::getProbabilidadExito));
        String expectedOutput="{50=[BAQ-01-18], 25=[BOG-02-18, BOG-03-18], 10=[BOG-01-18, BUC-04-18], 30=[BUC-01-18]}";
        assertEquals(expectedOutput,opptysPorExito.toString());
    }

    @Test
    public void pedidoMayorValor(){
        List<Pedido> listaPedidos=Arrays.asList(ped1,ped2,ped3,ped4,ped5);//puedo armarla dinámicamente
        double mayorFunc=op.pedidoPrecioMayor(listaPedidos);
        double mayorStream=listaPedidos.stream().mapToDouble(p->p.getValorPedido()).max().getAsDouble();
        assertEquals(mayorFunc,mayorStream);
    }

    @Test(expected=IllegalStateException.class)
    public void illegalStateException(){
        Stream<Pedido> streamPedidos=misOportunidades.stream().filter(op->op.getPedido().isPresent())
                .map(op->op.getPedido().get());
        streamPedidos.forEach(ped-> ped.getEmpleado());
        streamPedidos.collect(Collectors.averagingDouble(p->p.getValorPedido()));
    }

    @Test
    public void OportunidadesExitoMenora50(){
         Map<Boolean,List<Oportunidad>> opptysMenora50=misOportunidades.stream()
                 .filter(op->op.getPedido().isPresent())
                .collect(Collectors.partitioningBy(op->op.getProbabilidadExito()<50));
        assertEquals(3,opptysMenora50.get(true).stream().count());
    }

    @Test
    public void empleadoSololetras(){
        String regex="^[a-zA-Z ]+$";
        List<Pedido> pedidos=Arrays.asList(ped1,ped2,ped3,ped4,ped5);
        boolean matches=pedidos.stream()
                .allMatch(p->p.getEmpleado().get().matches(regex));
        assertTrue(matches);
    }
}
