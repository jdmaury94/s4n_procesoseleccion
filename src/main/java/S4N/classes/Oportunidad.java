package S4N.classes;

import java.util.Optional;

public class Oportunidad {

    String numOportunidad;
    String nombreVendedor;
    double valorOportunidad;
    int probExito;
    Pedido pedido;

    public Oportunidad(){

    }
    public Oportunidad(String numOportunidad,String nombreVendedor,double valorOportunidad,int probExito){
        this.numOportunidad=numOportunidad;
        this.nombreVendedor=nombreVendedor;
        this.valorOportunidad=valorOportunidad;
        this.probExito=probExito;
    }

    public Oportunidad(String numOportunidad,String nombreVendedor,double valorOportunidad,int probExito,Pedido pedido){
        this.numOportunidad=numOportunidad;
        this.nombreVendedor=nombreVendedor;
        this.valorOportunidad=valorOportunidad;
        this.probExito=probExito;
        this.pedido=pedido;
    }

    @Override
    public String toString(){
        return numOportunidad;
    }
    public String getNumOportunidad() {
        return numOportunidad;
    }

    public void setNumOportunidad(String numOportunidad) {
        this.numOportunidad = numOportunidad;
    }

    public String getNombreVendedor() {
        return nombreVendedor;
    }

    public void setNombreVendedor(String nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    }

    public double getValorOportunidad() {
        return valorOportunidad;
    }

    public void setValorOportunidad(double valorOportunidad) {
        this.valorOportunidad = valorOportunidad;
    }

    public int getProbabilidadExito() {
        return probExito;
    }

    public void setProbabilidadExito(int probabilidadExito) {
        this.probExito = probabilidadExito;
    }

    public Optional<Pedido> getPedido() {
        return Optional.ofNullable(pedido);
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }
}
