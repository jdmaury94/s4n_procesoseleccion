package S4N.classes;

import S4N.interfaces.Operacion;

import java.util.ArrayList;
import java.util.List;

public class Operaciones{

    public ArrayList<String> empToMayus(ArrayList<Oportunidad> oportunidades,Operacion operacion){
        ArrayList<String> vendedores=new ArrayList<>();
        for(Oportunidad op:oportunidades){
            String nombreVendedor=op.getNombreVendedor();
            vendedores.add(operacion.convertirAMayuscula(nombreVendedor));
        }

        return vendedores;

    }

    public double pedidoPrecioMayor(List<Pedido> listaPedido){
        double mayor=0;
        for(Pedido ped:listaPedido){
            if(ped.getValorPedido()>mayor)
                mayor=ped.getValorPedido();
        }
        return mayor;
    }

}
