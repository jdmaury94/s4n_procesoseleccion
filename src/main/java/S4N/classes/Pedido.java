package S4N.classes;

import java.util.Optional;

public class Pedido {

    String numPedido;
    double valorPedido;
    String empleado;

    public Pedido(String numPedido,double valorPedido, String empleado){
        this.numPedido=numPedido;
        this.valorPedido=valorPedido;
        this.empleado=empleado;
    }
    public String getNumPedido() {
        return numPedido;
    }

    public double getValorPedido() {
        return valorPedido;
    }

    public Optional<String> getEmpleado() {
        return Optional.ofNullable(empleado);
    }

}
