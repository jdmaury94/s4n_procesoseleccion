package S4N.interfaces;

import S4N.classes.Oportunidad;

import java.util.ArrayList;

@FunctionalInterface
public interface Operacion {
    String convertirAMayuscula(String nombreVendedor);
}
